package com.paradisegroup.app.register.views.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.model.Room;

import java.util.List;

/**
 * Adapter that fills the list of available rooms.
 * Created by Fiorella.
 */
public class AvailableRoomAdapter extends ArrayAdapter<Room> {

    private Activity activity;
    private List<Room> data;

    public AvailableRoomAdapter(Activity activity, int resource, List<Room> data) {
        super(activity, resource, data);
        this.activity = activity;
        this.data = data;
    }

    /**
     * Return the view that corresponds the object at a certain position in list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflator = activity.getLayoutInflater();
            convertView = inflator.inflate(R.layout.list_element_available_room, null);
        }

        TextView nr = (TextView) convertView.findViewById(R.id.room_nr);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        TextView price = (TextView) convertView.findViewById(R.id.price);

        if (position == 0) {
            if (nr != null) nr.setText("Nr.");
            if (description != null) description.setText("Beskrivning");
            if (price != null) price.setText("Pris");
            convertView.setBackgroundResource(R.color.primary);
        } else {


            if (nr != null) nr.setText(String.valueOf(data.get(position).getNumber()));
            if (description != null) description.setText(data.get(position).getDescription());
            if (price != null) price.setText(data.get(position).getPrice() + "kr");
        }

        return convertView;
    }
}
