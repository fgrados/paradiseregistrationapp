package com.paradisegroup.app.register.views;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.WeatherFetch;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

//    private TextView text;
    private String textString = "";
    private TextView weather;
    private Typeface weatherFont;
    private TextView weatherIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        updateFragment(0);
        updateFragment(1);
        updateFragment(2);

        weatherFont = Typeface.createFromAsset(this.getAssets(), "weatherfont.ttf");

        weather = (TextView) findViewById(R.id.weather);
        weatherIcon = (TextView) findViewById(R.id.weather_icon);
//        text = (TextView) findViewById(R.id.employees);
//
//        text.setText(textString);
        new FetchWeather().execute();


    }

    private void setWeather(JSONObject json) {
        try {
            String tmp = json.getJSONObject("main").getString("temp") + " ℃";
            weather.setText(tmp);

            int id = json.getJSONArray("weather").getJSONObject(0).getInt("id");
            int sunrise = json.getJSONObject("sys").getInt("sunrise");
            int sunset = json.getJSONObject("sys").getInt("sunset");
            setWeatherIcon(id, sunrise, sunset);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        weatherIcon.setTypeface(weatherFont);
    }

    public void updateFragment(int fragment){
        switch (fragment){
            case 0:
                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.available, new AvailableFragment());
                ft.commit();
                break;
            case 1:
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.unavailable, new UnavailableFragment());
                ft.commit();
                break;
            case 2:
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.to_clean, new ToClean());
                ft.commit();
                break;
        }
    }
    private void setWeatherIcon(int id, long sunrise, long sunset) {
        String icon = "";

        if (id == 800) {
            // Clear sky
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
                icon = this.getString(R.string.weather_sunny);
            } else {
                icon = this.getString(R.string.weather_clear_night);
            }
        } else {
            id = id / 100;
            switch (id) {
                case 2:
                    icon = this.getString(R.string.weather_thunder);
                    break;
                case 3:
                    icon = this.getString(R.string.weather_drizzle);
                    break;
                case 5:
                    icon = this.getString(R.string.weather_rainy);
                    break;
                case 6:
                    icon = this.getString(R.string.weather_snowy);
                    break;
                case 7:
                    icon = this.getString(R.string.weather_foggy);
                    break;
                case 8:
                    icon = this.getString(R.string.weather_cloudy);
                    break;

            }
            weatherIcon.setText(icon);
        }
    }

    private class GetData extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                //10.0.2.2
                URL url = new URL("http://192.168.1.96:8080/employees/list");
                HttpURLConnection client = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(client.getInputStream());
                StringBuilder sb = new StringBuilder();
                String response = "";

                int letter;
                while ((letter = in.read()) != -1) {
                    sb.append((char) letter);

                }

                textString = (sb.toString());
            } catch (MalformedURLException e) {
                textString = ("malformed exception!......................");
                System.out.println("malformed exception!......................");
                e.printStackTrace();
            } catch (IOException e) {
                textString = ("IO Exception");
                System.out.println("IO Exception");
                e.printStackTrace();
            }
            return null;
        }
    }

    private class FetchWeather extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            WeatherFetch wf = new WeatherFetch();
            JSONObject json = wf.getJson(MainActivity.this);
            return json;
        }

        @Override
        protected void onPostExecute(Object json) {

            super.onPostExecute(json);
            if ((json) != null) {
                setWeather((JSONObject) json);
            }
        }
    }
}
