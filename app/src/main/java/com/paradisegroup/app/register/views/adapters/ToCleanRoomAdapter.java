package com.paradisegroup.app.register.views.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.paradisegroup.app.register.JsonConverter;
import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.model.Register;

import java.util.List;

/**
 * Adapter that fills the list of register that have already checked out and wait to be cleaned.
 * Created by Fiorella.
 */
public class ToCleanRoomAdapter extends ArrayAdapter<Register> {

    private Activity activity;
    private List<Register> data;

    public ToCleanRoomAdapter(Activity activity, int resource, List<Register> data) {
        super(activity, resource, data);
        this.activity = activity;
        this.data = data;
    }

    /**
     * Return the view that corresponds the object at a certain position in list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflator = activity.getLayoutInflater();
            convertView = inflator.inflate(R.layout.list_element_room_toclean, null);
        }

        TextView nr = (TextView) convertView.findViewById(R.id.room_nr);
        TextView checkout = (TextView) convertView.findViewById(R.id.checkout);

        if (position == 0) {
            if (nr != null) nr.setText("Nr.");
            if (checkout != null) checkout.setText("Checkout");
            convertView.setBackgroundResource(R.color.primary);
        } else {


            if (nr != null) nr.setText(String.valueOf(data.get(position).getRoom().getNumber()));
            if (checkout != null)
                checkout.setText(data.get(position).getCheck_out().toString(JsonConverter.fmt));
        }

        return convertView;
    }

}
