package com.paradisegroup.app.register.model;

import java.io.Serializable;

/**
 * Model class for Room.
 * Created by Fiorella.
 */
public class Room implements Serializable {

    private int id;

    private int number;

    private String description;

    private boolean available;

    private int price;

    /**
     * Setter & getter
     */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
