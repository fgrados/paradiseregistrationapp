package com.paradisegroup.app.register.model;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Model class for Register
 * Created by Fiorella.
 */

public class Register implements Serializable {

    private int id;

    private Room room;

    private DateTime check_in;

    private DateTime check_out;

    private String passengerName;

    private String passengerLastname;

    private String dni;

    private String address;

    private boolean active;

    /**
     * Setter & getter
     */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public DateTime getCheck_in() {
        return check_in;
    }

    public void setCheck_in(DateTime check_in) {
        System.out.println(check_in.toString());
        this.check_in = check_in;
    }

    public DateTime getCheck_out() {
        return check_out;
    }

    public void setCheck_out(DateTime check_out) {
        this.check_out = check_out;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerLastname() {
        return passengerLastname;
    }

    public void setPassengerLastname(String passengerLastname) {
        this.passengerLastname = passengerLastname;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
