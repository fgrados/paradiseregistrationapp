package com.paradisegroup.app.register.views.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.model.Room;

import java.util.List;

/**
 * Created by Fiorella on 15/04/2016.
 */
public class AvailableRoomSpinnerAdapter extends ArrayAdapter<Room> {

    private Activity activity;
    private List<Room> data;

    public AvailableRoomSpinnerAdapter(Activity activity, int resource, List<Room> data) {
        super(activity, resource, data);
        this.activity = activity;
        this.data = data;
    }


    /**
     * Return the view that corresponds the object at a certain position in the spinner's list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflator = activity.getLayoutInflater();
            convertView = inflator.inflate(R.layout.spinner_room_nr, null);
        }

        TextView nr = (TextView) convertView.findViewById(R.id.room_nr_spinner);

        if (position == 0) {
            if (nr != null) nr.setText("Nr.");
        } else {
            if (nr != null) nr.setText(String.valueOf(data.get(position).getNumber()));
        }

        return convertView;
    }


    /**
     * Return the dropdown view that corresponds the object at a certain position in spinner.
     */
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
