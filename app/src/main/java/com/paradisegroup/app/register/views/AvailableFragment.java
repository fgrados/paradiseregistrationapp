package com.paradisegroup.app.register.views;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.paradisegroup.app.register.JsonConverter;
import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.RegisterConnector;
import com.paradisegroup.app.register.RoomConnector;
import com.paradisegroup.app.register.model.Register;
import com.paradisegroup.app.register.model.Room;
import com.paradisegroup.app.register.views.adapters.AvailableRoomAdapter;

import java.util.List;

/**
 * Fragment shows a list with available rooms their descriptions and price
 * A simple {@link Fragment} subclass.
 */
public class AvailableFragment extends Fragment {

    private ListView list;
    private ArrayAdapter<String> adapter;
    private List<Room> roomList;

    public AvailableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_available, container, false);

        list = (ListView) rootview.findViewById(R.id.available_room_list);
        new RoomsFetcher().execute();

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        adapter.add("Checkin");
        adapter.add("Att städa");

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    showOptions(position);
                }
            }
        });

        return rootview;
    }

    /**
     * show options of the room in list.
     * Options: Checkin, to clean.
     * Reload fragments if necesary.
     *
     * @param index
     */
    private void showOptions(final int index) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                switch (position) {
                    case 0:
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        CheckinFragment fragment = new CheckinFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("index", index);
                        fragment.setArguments(bundle);
                        ft.replace(R.id.available, fragment);
                        ft.commit();
                        break;
                    case 1:
                        new Unclean().execute(index);
                        break;
                }
            }
        });
        builder.show();

    }

    private void setAdapter(List<Room> data) {
        roomList = data;
        list.setAdapter(new AvailableRoomAdapter(getActivity(), R.layout.list_element_available_room, roomList));

    }

    /**
     * Gets in background a list with all available rooms.
     * and set an adapter if request was successful.
     */
    private class RoomsFetcher extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            RoomConnector sc = new RoomConnector();
            String json = sc.getAvailableRoom(true);
            System.out.println(json);
            return json;
        }

        @Override
        protected void onPostExecute(Object json) {
            List<Room> availables = JsonConverter.roomToList((String) json);

            if (availables != null) {
                availables.add(0, new Room());
                setAdapter(availables);
            }
        }
    }

    /**
     * This class mark a room as not cleaned.
     */
    private class Unclean extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            int pos = (int) params[0];

            RoomConnector roomConnector = new RoomConnector();

            RegisterConnector rc = new RegisterConnector();
            String regJson = rc.getLastRegByRoomId(roomList.get(pos).getId());

            Register reg = JsonConverter.toRegister(regJson);
            boolean response = false;
            if (reg != null) {
                response = rc.closeRegister(reg.getId());
            }

            if (response) {
                response = roomConnector.changeAvailability(roomList.get(pos));
            }
            return response;
        }

        @Override
        protected void onPostExecute(Object o) {
            // If the room was marked as not clean then reload fragments
            if ((boolean) o) {
                ((MainActivity) getActivity()).updateFragment(0);
                ((MainActivity) getActivity()).updateFragment(2);
                Toast.makeText(getContext(), "new room to clean.", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getContext(), "Room could not be mark to be cleaned", Toast.LENGTH_LONG).show();
            }

        }
    }
}
