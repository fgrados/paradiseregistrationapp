package com.paradisegroup.app.register.views;


import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.paradisegroup.app.register.JsonConverter;
import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.RegisterConnector;
import com.paradisegroup.app.register.RoomConnector;
import com.paradisegroup.app.register.model.Register;
import com.paradisegroup.app.register.views.adapters.ToCleanRoomAdapter;

import java.util.List;

/**
 * Fragment shows a list with all rooms to be cleaned and mos recent checkout.
 * A simple {@link Fragment} subclass.
 */
public class ToClean extends Fragment {

    private ListView list;
    private ArrayAdapter<String> adapter;
    private List<Register> registers;


    public ToClean() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_to_clean, container, false);

        list = (ListView) rootview.findViewById(R.id.list_to_clean);

        new ToCleanFetcher().execute();

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        adapter.add("Städat");

        return rootview;
    }


    /**
     * Set adapter to fill the list.
     */
    private void setAdapter(List<Register> registers) {
        registers.add(0, new Register());
        this.registers = registers;
        list.setAdapter(new ToCleanRoomAdapter(getActivity(), R.layout.list_element_unavailable_room, registers));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    showDialog(position);
                }
            }
        });
    }

    /**
     * show options: clean ( mark room as cleaned)
     *
     * @param position
     */
    private void showDialog(final int position) {
        AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(getActivity());
        dialogbuilder.setCancelable(true);
        dialogbuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setCancelable(true);
                switch (index) {
                    case 0:
                        alert.setMessage("Mark as clean?");
                        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new Clean().execute(position);
                            }
                        });
                        alert.show();
                        break;

                }

            }
        });
        dialogbuilder.show();
    }

    /**
     * Class runs in background
     * Gets all rooms that need to be cleaned
     * if success setAdapted method is called.
     */
    private class ToCleanFetcher extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            RegisterConnector rc = new RegisterConnector();
            String json = rc.getInactiveRegisters();

            return json;
        }

        @Override
        protected void onPostExecute(Object o) {
            String json = (String) o;
            if (json != null) {
                setAdapter(JsonConverter.registerToList(json));
            }
        }
    }

    /**
     * Runs in background
     * send request to mark a room as clean.
     * refresh fragments if succes.
     */
    private class Clean extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            int pos = (int) params[0];
            RoomConnector rc = new RoomConnector();
            boolean response = rc.changeAvailability(registers.get(pos).getRoom());
            if (response) {
                // Set registration as inactive
                RegisterConnector regConn = new RegisterConnector();
                return regConn.closeRegister(registers.get(pos).getId());
            }
            return false;
        }

        @Override
        protected void onPostExecute(Object o) {

            ((MainActivity) getActivity()).updateFragment(0);
            ((MainActivity) getActivity()).updateFragment(2);

            boolean success = (boolean) o;

            if (success) {
                Toast.makeText(getContext(), "Room marked as clean.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "Couldn't mark room as clean.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
