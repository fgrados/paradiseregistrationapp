package com.paradisegroup.app.register;

import com.google.gson.Gson;
import com.paradisegroup.app.register.model.Register;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Makes all register requests to server
 * Created by Fiorella.
 */
public class RegisterConnector {

    private String ip = "http://192.168.43.75:8080";

    /**
     * Request all active registers
     *
     * @return json with all active registers
     */
    public String getActiveRegisters() {
        try {
            URL url = new URL(ip + "/register/actives");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            StringBuilder sb = new StringBuilder();
            int letter;
            while ((letter = in.read()) != -1) {
                sb.append((char) letter);

            }
            conn.disconnect();

            return sb.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Requests al reqgisters that checked out
     *
     * @return json with all registers
     */
    public String getInactiveRegisters() {
        try {
            URL url = new URL(ip + "/register/inactives");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            StringBuilder sb = new StringBuilder();
            int letter;
            while ((letter = in.read()) != -1) {
                sb.append((char) letter);
            }
            conn.disconnect();
            return sb.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Send a post request with a new register
     *
     * @param register
     * @return true if success, false otherwise
     */
    public boolean postRegister(Register register) {
        Gson gson = new Gson();

        try {
            URL url = new URL(ip + "/register/new");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);

            DataOutputStream out = new DataOutputStream((connection.getOutputStream()));
            out.write(("register=" + gson.toJson(register)).getBytes());
            out.flush();
            out.close();

            System.out.println("POST RESPONSE CODE...........................:" +connection.getResponseCode());

            connection.disconnect();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Send a Put request to checkout a register
     *
     * @param register
     * @return true if success.
     */
    public boolean checkoutRegister(Register register) {

        try {
            URL url = new URL(ip + "/register/updatereg/" + register.getId());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("PUT");
            connection.setDoOutput(true);

            connection.connect();
            System.out.println("CHECKOUT RESPONSE CODE...........................:" +connection.getResponseCode());

            connection.disconnect();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Send a post request to update a register
     *
     * @param register
     * @return true if success.
     */
    public boolean updateRegister(Register register) {

        Gson gson = new Gson();

        try {
            URL url = new URL(ip + "/register/update/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);

            connection.connect();

            DataOutputStream out = new DataOutputStream((connection.getOutputStream()));
            String s = gson.toJson(register);
            out.write(("register=" + s).getBytes());
            out.flush();
            out.close();

            connection.disconnect();

            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * GET Request to get the las register of a room.
     *
     * @param roomID
     * @return json of the register object
     */
    public String getLastRegByRoomId(int roomID) {

        try {
            URL url = new URL(ip + "/register/lastreg/" + roomID);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            StringBuilder sb = new StringBuilder();
            int letter;
            while ((letter = in.read()) != -1) {
                sb.append((char) letter);
            }
            conn.disconnect();

            return sb.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Send DELETE Request to delete a register by id
     *
     * @param register
     * @return true if success.
     */
    public boolean deleteRegister(Register register) {
        Gson gson = new Gson();

        try {
            URL url = new URL(ip + "/register/delete/" + register.getId());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("DELETE");
            connection.setDoOutput(true);

            DataOutputStream out = new DataOutputStream((connection.getOutputStream()));
            out.write(("reg=" + gson.toJson(register)).getBytes());
            out.flush();
            out.close();

            connection.disconnect();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Send a request to set a register as inactive.
     *
     * @param id - register id
     * @return tru if success
     */
    public boolean closeRegister(int id) {


        String urlString = ip + "/register/close/" + id;
        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.connect();

            connection.disconnect();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
