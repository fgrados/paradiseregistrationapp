package com.paradisegroup.app.register.views;


import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.paradisegroup.app.register.JsonConverter;
import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.RegisterConnector;
import com.paradisegroup.app.register.model.Register;
import com.paradisegroup.app.register.views.adapters.UnavailableRoomAdapter;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;

import java.util.List;

/**
 * Shows list med unavailable rooms and checkin date
 * A simple {@link Fragment} subclass.
 */
public class UnavailableFragment extends Fragment {

    private ListView listView;
    private ArrayAdapter<String> adapter;
    private List<Register> list;

    private WebView mWebView;

    public UnavailableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_unavailable, container, false);

        listView = (ListView) rootview.findViewById(R.id.unavailable);

        new UnavailableFetcher().execute();

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        adapter.add("Checkout");
        adapter.add("Ändra bokning");
        adapter.add("Ta bort bokning");

        return rootview;
    }

    private void setAdapter(List<Register> unavailables) {
        list = unavailables;
        listView.setAdapter(new UnavailableRoomAdapter(getActivity(), R.layout.list_element_unavailable_room, list));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0) showDialog(position);

            }
        });
    }

    /**
     * Show options of the register
     * Options: Checkout, edit register, delete register.
     */
    private void showDialog(final int position) {
        AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(getActivity());
        dialogbuilder.setCancelable(true);
        dialogbuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setCancelable(true);
                switch (index) {
                    case 0:
                        alert.setMessage("Vill du checka ut nu?");
                        alert.setPositiveButton("Check-out!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new CheckOut().execute(position);
                            }
                        });
                        showPrintDialog(position, alert);
                        break;
                    case 1:
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("register", list.get(position));
                        CheckinFragment fragment = new CheckinFragment();
                        fragment.setArguments(bundle);
                        ft.replace(R.id.available, fragment);
                        ft.commit();
                        break;
                    case 2:
                        alert.setMessage("Ta bort bokning?");
                        alert.setPositiveButton("Ta bort", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new Delete().execute(position);
                            }
                        });
                        alert.show();
                        break;
                }

            }
        });
        dialogbuilder.show();
    }

    /**
     * Show dialog to print receipt.
     *
     * @param pos
     * @param alert
     */
    private void showPrintDialog(final int pos, final AlertDialog.Builder alert) {
        final Register reg = list.get(pos);
        reg.setCheck_out(new DateTime());
        final AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(getActivity());
        dialogbuilder.setCancelable(true);
        dialogbuilder.setMessage("Vill du skriva ut kvittot och checka-ut?");
        dialogbuilder.setNegativeButton("Checka ut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new CheckOut().execute(pos);
            }
        });
        dialogbuilder.setPositiveButton("Check och Skriv ut", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doWebViewPrint(pos);
            }
        });
        dialogbuilder.show();
    }

    /**
     * Count nights of the register to be paid
     *
     * @param reg
     * @return days
     */
    private int countNights(Register reg) {
        DateTime in = reg.getCheck_in();
        if (in.getHourOfDay() < 11) in.minusDays(1);
        in = new DateTime(in.getYear(), in.getMonthOfYear(), in.getDayOfMonth(), 11, 00);

        DateTime out = reg.getCheck_out();
        if (out.getHourOfDay() > 11) out.plusDays(1);
        out = new DateTime(out.getYear(), out.getMonthOfYear(), out.getDayOfMonth(), 11, 00);

        int days = Days.daysBetween(in, out).getDays();
        return days;
    }

    private void doWebViewPrint(final int pos) {
        Register reg = list.get(pos);
        // Create a WebView object specifically for printing
        WebView webView = new WebView(getActivity());
        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //Log.i(TAG, "page finished loading " + url);
                createWebPrintJob(view, pos);
                mWebView = null;
            }
        });

        // Generate an HTML document on the fly:
        String htmlDocument = "<html>\n" +
                "<head>\n" +
                "<style>\n" +
                "table.lamp {\n" +
                "width:60%;\n" +
                "margin: 0 auto;\n" +
                "border:3px solid #000000;\n" +
                "}\n" +
                "table.lamp th, td {\n" +
                "padding:10px;\n" +
                "}\n" +
                "\n" +
                "div.table{\n" +
                "padding-top: 20px;\n" +
                "width: 100%;\n" +
                "height: 170px;\n" +
                "}\n" +
                "div.header {\n" +
                "padding-top: 30px;\n" +
                "width: 100%;\n" +
                "}\n" +
                "\n" +
                "img.logo{\n" +
                "display: block;\n" +
                "margin-left: auto;\n" +
                "margin-right: auto\n" +
                "}\n" +
                ".title {\n" +
                "width: 60%;\n" +
                "align: center;\n" +
                "margin: 0 auto;\n" +
                "font-weight: bold;\n" +
                "}\n" +
                "\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"header\">\n" +
                "<img class=\"logo\" src=\"logo.jpg\">\n" +
                "<p class=\"title\" style=\"margin-top:20px\"> Kvitto </p>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"table\">\n" +
                "<table class=\"lamp\">\n" +
                "<tr>\n" +
                "<th>\n" +
                "Namn:\n" +
                "</th>\n" +
                "<td>\n" +
                reg.getPassengerName() + " " + reg.getPassengerLastname() + "\n" +
                "</td>\n" +
                "<th>\n" +
                "Rum nr:\n" +
                "</th>\n" +
                "<td>\n" +
                reg.getRoom().getNumber() + "\n" +
                "</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<th>\n" +
                "Adress:\n" +
                "</th>\n" +
                "<td>\n" +
                reg.getAddress() + "\n" +
                "</td>\n" +
                "<th>\n" +
                "Check-in:\n" +
                "</th>\n" +
                "<td>\n" +
                reg.getCheck_in().toString(JsonConverter.fmt) + "\n" +
                "</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<th>\n" +
                "Datum:\n" +
                "</th>\n" +
                "<td>\n" +
                new DateTime().toString(DateTimeFormat.forPattern("dd/MM/yyyy")) + "\n" +
                "</td>\n" +
                "<th>\n" +
                "Checkout:\n" +
                "</th>\n" +
                "<td>\n" +
                reg.getCheck_out().toString(JsonConverter.fmt) + "\n" +
                "</td>\n" +
                "</tr>\n" +
                "</table>\n" +
                "</div>\n" +
                "\n" +
                "<div class=\"table\">\n" +
                "<p class=\"title\" style=\"margin-bottom:20px\"> Detaljer: </p>\n" +
                "<table class=\"lamp\" style=\"text-align: left;\">\n" +
                "<tr>\n" +
                "<th>\n" +
                "Antal nätter\n" +
                "</th>\n" +
                "<th>\n" +
                "Pris(sek)/natt\n" +
                "</th>\n" +
                "<th>\n" +
                "Rum nr.\n" +
                "</th>\n" +
                "<th>\n" +
                "Summa \n" +
                "</th>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td>\n" +
                countNights(reg) + "\n" +
                "</td>\n" +
                "<td>\n" +
                reg.getRoom().getPrice() + "\n" +
                "</td>\n" +
                "<td>\n" +
                reg.getRoom().getNumber() + "\n" +
                "</td>\n" +
                "<td>\n" +
                (countNights(reg) * reg.getRoom().getPrice()) + "\n" +
                "</td>\n" +
                "</tr>\n" +
                "</table>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>\n";
        webView.loadDataWithBaseURL("file:///android_asset/logo.jpg", htmlDocument, "text/HTML", "UTF-8", null);

        // Keep a reference to WebView object until you pass the PrintDocumentAdapter
        // to the PrintManager
        mWebView = webView;

    }

    private void createWebPrintJob(WebView webView, int pos) {

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager) getActivity()
                .getSystemService(Context.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        // Create a print job with name and adapter instance
        String jobName = getString(R.string.app_name) + " Document";
        PrintJob printJob = printManager.print(jobName, printAdapter,
                new PrintAttributes.Builder().build());

        new CheckOut().execute(pos);
    }

    /**
     * Runs in background
     * tries to get all active registers that didn't check out yet.
     * SetAdapter method is called if request success.
     */
    private class UnavailableFetcher extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            RegisterConnector sc = new RegisterConnector();
            String json = sc.getActiveRegisters();
            return json;
        }

        @Override
        protected void onPostExecute(Object json) {
            List<Register> unavailables = JsonConverter.registerToList((String) json);

            if (unavailables != null) {
                unavailables.add(0, new Register());
                setAdapter(unavailables);
            }
        }
    }

    /**
     * Runs in background
     * Tries to checkout a register.
     * Reload fragments if success.
     */
    private class CheckOut extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            int pos = (int) params[0];
            Register reg = list.get(pos);

            RegisterConnector rc = new RegisterConnector();

            return rc.checkoutRegister(reg);
        }

        @Override
        protected void onPostExecute(Object o) {
            if ((boolean) o) {
                ((MainActivity) getActivity()).updateFragment(1);
                ((MainActivity) getActivity()).updateFragment(2);
                Toast.makeText(getContext(), "checked out sucssesfully!", Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(getContext(), "failed to check-out!", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Runs in background
     * Send a request to delete a register
     * if success reload fragments
     */
    private class Delete extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            int pos = (int) params[0];
            Register reg = list.get(pos);
            RegisterConnector rc = new RegisterConnector();
            return rc.deleteRegister(reg);
        }

        @Override
        protected void onPostExecute(Object o) {
            if ((boolean) o) {
                Toast.makeText(getContext(), "checked out sucssesfully!", Toast.LENGTH_LONG).show();
                ((MainActivity) getActivity()).updateFragment(0);
                ((MainActivity) getActivity()).updateFragment(1);
            } else {
                Toast.makeText(getContext(), "failed to check-out!", Toast.LENGTH_LONG).show();

            }
        }
    }
}
