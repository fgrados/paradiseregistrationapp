package com.paradisegroup.app.register;

import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Fetches temperature in stockholm
 * Created by Fiorella.
 */
public class WeatherFetch {

    private static final String OPEN_WEATHER_MAP_API = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric";

    public static JSONObject getJson(Context context) {

        try {
            URL url = new URL(String.format(OPEN_WEATHER_MAP_API, "Stockholm"));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.addRequestProperty("x-api-key", context.getString(R.string.api_id));

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            StringBuffer json = new StringBuffer(1024);
            String tmp = "";
            while ((tmp = reader.readLine()) != null) {
                json.append(tmp).append("\n");

            }

            JSONObject data = (new JSONObject(json.toString()));

            reader.close();
            if (data.getInt("cod") != 200) {
                return null;
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
