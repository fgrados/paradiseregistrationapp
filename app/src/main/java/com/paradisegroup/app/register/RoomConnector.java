package com.paradisegroup.app.register;

import com.paradisegroup.app.register.model.Room;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Makes all register room to server
 * Created by Fiorella.
 */
public class RoomConnector {

    private String ip = "http://192.168.43.75:8080";

    /**
     * Send a GET Request to get all the available rooms.
     *
     * @param available
     * @return Json with all rooms.
     */
    public String getAvailableRoom(boolean available) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(ip+"/rooms/available?available=" + available);
            conn = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            StringBuilder sb = new StringBuilder();
            int letter;
            while ((letter = in.read()) != -1) {
                sb.append((char) letter);

            }
            conn.disconnect();
            return sb.toString();
        } catch (MalformedURLException e) {
            if (conn != null) conn.disconnect();
            e.printStackTrace();
        } catch (IOException e) {
            if (conn != null) conn.disconnect();
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Send put request to change availability of a room by id.
     *
     * @param room
     * @return true if success
     */
    public boolean changeAvailability(Room room) {

        HttpURLConnection conn = null;
        try {
            URL url = new URL(ip + "/rooms/clean/" + room.getId());
            conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("PUT");

            conn.connect();
            System.out.println("CLEAN ROOM RESPONSE CODE...........................:" +conn.getResponseCode());

            conn.disconnect();
            return true;
        } catch (MalformedURLException e) {
            if (conn != null) conn.disconnect();
            e.printStackTrace();
        } catch (IOException e) {
            if (conn != null) conn.disconnect();
            e.printStackTrace();
        }

        return false;
    }
}
