package com.paradisegroup.app.register.views.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.paradisegroup.app.register.JsonConverter;
import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.model.Register;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Adapter fills the list view with active registers.
 * Created by Fiorella.
 */
public class UnavailableRoomAdapter extends ArrayAdapter<Register> {

    private Activity activity;
    private List<Register> data;

    public UnavailableRoomAdapter(Activity activity, int resource, List<Register> data) {
        super(activity, resource, data);
        this.activity = activity;
        this.data = data;
    }

    /**
     * Return the view that corresponds the object at a certain position in list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflator = activity.getLayoutInflater();
            convertView = inflator.inflate(R.layout.list_element_unavailable_room, null);
        }

        TextView nr = (TextView) convertView.findViewById(R.id.room_nr);
        TextView in = (TextView) convertView.findViewById(R.id.checkin);
        TextView out = (TextView) convertView.findViewById(R.id.checkout);

        if (position > 0) {

            if (nr != null) nr.setText(String.valueOf(data.get(position).getRoom().getNumber()));
            if (in != null)
                in.setText(data.get(position).getCheck_in().toString(JsonConverter.fmt));
            DateTime outDatetime = data.get(position).getCheck_out();
            if (out != null && outDatetime != null)
                out.setText(outDatetime.toString(JsonConverter.fmt));
            else out.setText("-");
        } else {
            convertView.setBackgroundResource(R.color.primary);
        }

        return convertView;
    }
}
