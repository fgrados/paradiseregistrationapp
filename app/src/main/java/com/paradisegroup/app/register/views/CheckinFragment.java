package com.paradisegroup.app.register.views;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.paradisegroup.app.register.JsonConverter;
import com.paradisegroup.app.register.R;
import com.paradisegroup.app.register.RegisterConnector;
import com.paradisegroup.app.register.RoomConnector;
import com.paradisegroup.app.register.model.Register;
import com.paradisegroup.app.register.model.Room;
import com.paradisegroup.app.register.views.adapters.AvailableRoomSpinnerAdapter;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

/**
 * shows a form to fill to create a new register or update an active one.
 * A simple {@link Fragment} subclass.
 */
public class CheckinFragment extends Fragment {

    private Spinner rooms;
    private EditText in;
    private EditText out;
    private EditText name;
    private EditText lastname;
    private EditText dni;
    private EditText address;

    private Button submit;
    private Button cancel;

    private Register register;
    private Room room;
    private List<Room> roomList;

    private boolean updating = false;

    public CheckinFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_checkin, container, false);

        // find all elements in layout
        rooms = (Spinner) rootview.findViewById(R.id.rooms);
        in = (EditText) rootview.findViewById(R.id.check_in);
        out = (EditText) rootview.findViewById(R.id.checkout);
        name = (EditText) rootview.findViewById(R.id.name);
        lastname = (EditText) rootview.findViewById(R.id.lastname);
        dni = (EditText) rootview.findViewById(R.id.dni);
        address = (EditText) rootview.findViewById(R.id.address);

        submit = (Button) rootview.findViewById(R.id.submit);
        cancel = (Button) rootview.findViewById(R.id.cancel);


        // Check if it is a new register or an update
        Bundle bundle = this.getArguments();
        if ((register = (Register) bundle.getSerializable("register")) != null) {
            // update
            setValuesFromBundle();
        } else {
            //new register
            register = new Register();
            register.setActive(true);
            room = new Room();
        }
        // fill spinner with available room numbers.
        new SpinnerFyller().execute();

        // if a room number of spinner is selected that room is assigned to the new/updated register.
        rooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    register.setRoom(roomList.get(position));
                } else {
                    register.setRoom(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                register.setRoom(null);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValues();
                if (isValid()) {
                    System.out.println(!isValid());
                    new RegisterPost().execute();
                }
            }
        });

        /**
         * reload available fragment if cancel button is clicked.
         */
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).updateFragment(0);
            }
        });
        return rootview;
    }

    /**
     * Called if register will be updated.
     * set values in place from data of the existent register to be edited.
     */
    private void setValuesFromBundle() {

        address.setText(register.getAddress());
        in.setText(register.getCheck_in().toString(JsonConverter.fmt));
        dni.setText(register.getDni());
        name.setText(register.getPassengerName());
        lastname.setText(register.getPassengerLastname());

        submit.setText("spara ändringar");
        updating = true;
    }

    /**
     * Set values from form to register object that wants to be save or updated.
     */
    private void setValues() {
        register.setAddress(address.getText().toString());
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
        register.setCheck_in(formatter.parseDateTime(in.getText().toString()));
        register.setDni(dni.getText().toString());
        register.setPassengerName(name.getText().toString());
        register.setPassengerLastname(lastname.getText().toString());
    }

    /**
     * check if the inserted data is valid.
     */
    private boolean isValid() {
        if (register.getDni() == null || register.getDni().length() < 8) return false;

        if (register.getAddress() == null || register.getAddress().length() < 10) return false;

        if (register.getPassengerName() == null || register.getPassengerName().length() < 3)
            return false;

        if (register.getPassengerLastname() == null || register.getPassengerLastname().length() < 3)
            return false;

        if (register.getCheck_in() == null) return false;

        if (register.getRoom() == null) return false;

        return true;
    }

    /**
     * set the adapter to the spinner to show the number of available rooms.
     * and choose the selected room.
     *
     * @param data
     */
    private void setAdapter(List<Room> data) {
        roomList = data;

        if (updating) roomList.add(register.getRoom());
        rooms.setAdapter(new AvailableRoomSpinnerAdapter(getActivity(), R.id.room_nr_spinner, data));
        int index = 0;
        if (updating) {
            index = roomList.size() - 1;
            rooms.setSelection(index);
        } else {
            Bundle bundle = getArguments();
            index = bundle.getInt("index");
            rooms.setSelection(index);
        }
        register.setRoom(roomList.get(index));
    }

    /**
     * Class rus in background and try to get data to fill the spinner
     * if success then call the setAdapter method.
     */
    private class SpinnerFyller extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            RoomConnector sc = new RoomConnector();

            return sc.getAvailableRoom(true);
        }

        @Override
        protected void onPostExecute(Object json) {
            List<Room> data = JsonConverter.roomToList((String) json);
            data.add(0, new Room());
            if (data != null) {
                setAdapter(data);
            }
        }
    }

    /**
     * Send a post request i background to post a new register or update and old one.
     * reload fragments id success.
     */
    private class RegisterPost extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            RegisterConnector rc = new RegisterConnector();
            if (updating) {
                return rc.updateRegister(register);
            }
            return rc.postRegister(register);
        }

        @Override
        protected void onPostExecute(Object success) {

            if ((boolean) success) {
                System.out.println("success");
                ((MainActivity) getActivity()).updateFragment(0);
                ((MainActivity) getActivity()).updateFragment(1);
            } else {
                Toast.makeText(getActivity(), "Det gick inte att checka in, försök igen!", Toast.LENGTH_LONG);
            }
        }
    }

}
