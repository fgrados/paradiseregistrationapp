package com.paradisegroup.app.register;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.paradisegroup.app.register.model.Register;
import com.paradisegroup.app.register.model.Room;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Class that coverts json into object or List of objects.
 * Created by Fiorella.
 */
public class JsonConverter {
    private static final String PATTERN = "dd/MM/yyyy HH:mm";
    public static final DateTimeFormatter fmt = DateTimeFormat.forPattern(PATTERN);

    public static List<Room> roomToList(String json) {
        Gson gson = new Gson();
        List<Room> list = gson.fromJson(json, new TypeToken<List<Room>>() {
        }.getType());
        return list;
    }

    public static List<Register> registerToList(String json) {

        final GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(DateTime.class, new DateTimeSerializer());
        final Gson gson = builder.create();

        List<Register> list = gson.fromJson(json, new TypeToken<List<Register>>() {
        }.getType());

        return list;
    }

    public static Register toRegister(String json) {

        final GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(DateTime.class, new DateTimeSerializer());
        final Gson gson = builder.create();
        return gson.fromJson(json, Register.class);
    }

    /**
     * Serializer that parse json into joda DateTime object.
     */
    public static class DateTimeSerializer implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {


        public JsonElement serialize(DateTime src, Type typeOfSrc, JsonSerializationContext context) {

            String retVal = fmt.print(src);
            return new JsonPrimitive(retVal);
        }

        public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();
            Long millis = (jsonObject.get("iMillis")).getAsLong();
            try {
                DateTime date = new DateTime(millis);
                return date;
            } catch (Exception e) {

                e.printStackTrace();
                return null;
            }
        }
    }
}
